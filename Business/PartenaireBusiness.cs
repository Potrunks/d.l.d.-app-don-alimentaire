﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GE.DataAccess;
using Fr.EQL.AI110.DLD_GE.Entities;

namespace Fr.EQL.AI110.DLD_GE.Business
{
    public class PartenaireBusiness
    {
        public List<PartenaireDetails> ToutPartenaireParVille(int idville)
        {
            PartenaireDAO pdao = new PartenaireDAO();
            return pdao.ToutPartenaireParVille(idville);
        }
        public List<PartenaireDetails> ToutPartenaireDetails()
        {
            PartenaireDAO pdao = new PartenaireDAO();
            return pdao.ToutPartenaireDetails();
        }
    }
}
