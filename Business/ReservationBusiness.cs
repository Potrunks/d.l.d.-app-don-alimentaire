﻿using Fr.EQL.AI110.DLD_GE.DataAccess;
using Fr.EQL.AI110.DLD_GE.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Business
{
    public class ReservationBusiness
    {
        public void SaveBasket(int idDon, int idAdh, DateTime dateAjoutPanier)
        {
            ReservationDAO reservationDAO = new ReservationDAO();
            reservationDAO.DeleteByIdAdh(idDon, idAdh);
            reservationDAO.Insert(idDon, idAdh, dateAjoutPanier);
        }
        public List<ReservationDetails> GetBasketByIdAdh(int id)
        {
            ReservationDAO reservationDAO = new ReservationDAO();
            return reservationDAO.GetAllProductsInBasketByIdAdh(id);
        }
        public List<ReservationDetails> GetBasketsWithNbArticles()
        {
            ReservationDAO dao = new ReservationDAO();
            return dao.GetAllBaskets();
        }
        public void EditDateReservation(int idReservation, int idDon, int idAdh)
        {
            ReservationDAO dao = new ReservationDAO();
            dao.Insert(idReservation);
            dao.Delete(idDon, idAdh);
        }
        public void DeleteDonInBasket(int idDon, int idAdh)
        {
            ReservationDAO dao = new ReservationDAO();
            dao.DeleteByIdAdh(idDon, idAdh);
        }
        public List<ReservationDetails> GetAllReservationByIdAdh(int idAdh)
        {
            List<ReservationDetails> list = new List<ReservationDetails>();
            ReservationDAO dao = new ReservationDAO();
            list = dao.GetAll(idAdh);
            return list;
        }
        public List<ReservationDetails> GetReservationsWithNbArticles()
        {
            ReservationDAO dao = new ReservationDAO();
            return dao.GetAllWithDateReservation();
        }
        public void EditDateAnnulation(int idReservation)
        {
            ReservationDAO dao = new ReservationDAO();
            dao.Update(idReservation);
        }
    }
}
