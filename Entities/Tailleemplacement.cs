﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Tailleemplacement
    {
        public int Idtaille { get; set; }
        public string Nom { get; set; }
        public string Dimension { get; set; }
    }
}
