﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Achatcoupon
    {
        public int Idachat { get; set; }
        public int Idadh { get; set; }
        public DateTime Dateachatcoupon { get; set; }
    }
}
