﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Categorie
    {
        public int IdCategorie { get; set; }
        public string Nom { get; set; }

        public Categorie()
        {
        }

        public Categorie(int idCategorie, string nom)
        {
            IdCategorie = idCategorie;
            Nom = nom;
        }
    }
}
