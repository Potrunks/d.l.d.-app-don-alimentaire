﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class InfrastockageDetails
    {
        public string Nomsociete { get; set; }
        public int Numrue { get; set; }
        public char? Complement { get; set; }
        public string Libelevoie { get; set; }
        public int NumruePart { get; set; }
        public char ComplementPart { get; set; }
        public string LibelevoiePart { get; set; }
        public int Codepostal { get; set; }
        public string Nom { get; set; }
        public int Idinfrastockage { get; set; }
        public int Numemplacement { get; set; }
        public int Idemplacementstockage { get; set; }
        public int NbEmpDispo { get; set; }
        public int Idpartenaire { get; set; }

    }
}
