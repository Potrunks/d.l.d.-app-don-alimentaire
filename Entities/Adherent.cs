﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Adherent
    {
        public int Idadh { get; set; }
        public int Idmotif { get; set; }
        public int Idville { get; set; }
        [Required(ErrorMessage = "Le numéro de voie est obligatoire")]
        public int Numrue { get; set; }
        public char Complement { get; set; }
        [Required(ErrorMessage = "Le libélé de voie est obligatoire")]
        public string Libelevoie { get; set; }
        [Required(ErrorMessage = "Le nom est obligatoire")]
        public string Nom { get; set; }
        [Required(ErrorMessage = "Le prénom est obligatoire")]
        public string Prenom { get; set; }
        [Required(ErrorMessage = "La civilité est obligatoire")]
        public string Civilite { get; set; }
        [Required(ErrorMessage = "La date de naissance est obligatoire")]
        public DateTime Datenaissance { get; set; }
        [Required(ErrorMessage = "Le numéro de téléphone est obligatoire")]
        public int Numtelephone { get; set; }
        [Required(ErrorMessage = "Le mail est obligatoire")]
        public string Mail { get; set; }
        public DateTime Datevalidationinscription { get; set; }
        public DateTime Dateinscription { get; set; }
        public string Login { get; set; }
        public string Motdepasse { get; set; }
        public DateTime Datedesinscription { get; set; }
        public DateTime Datedebutdesactivation { get; set; }
        public DateTime Datefindesactivation { get; set; }
        public int NbDon { get; set; }

        public Adherent()
        {
        }
    }
}
