﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class PartenaireDetails : Partenaire
    {
        public string Nomville { get; set; }
        public int PostalCode { get; set; }
        public List<Ville> Villes { get; set; }
        public List<PartenaireDetails> PartenairesDetails { get; set; }

        public PartenaireDetails()
        {
        }

        public PartenaireDetails(string nomville, int idville, List<Ville> villes, List<PartenaireDetails> partenaires)
        {
            Nomville = nomville;
            Idville = idville;
            Villes = villes;
            PartenairesDetails = partenaires;
        }

        public PartenaireDetails(Partenaire partenaire)
        {
            Idpartenaire = partenaire.Idpartenaire;
            Nomsociete = partenaire.Nomsociete;
            Idville = partenaire.Idville;
            Numrue = partenaire.Numrue;
            Libelevoie = partenaire.Libelevoie;



        }
    }
}
