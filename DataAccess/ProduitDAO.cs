﻿using Fr.EQL.AI110.DLD_GE.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.DataAccess
{
    public class ProduitDAO : DAO
    {
        public List<Produit> GetAll()
        {
            List<Produit> list = new List<Produit>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "select * from produit;";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Produit produit = new Produit();
                    produit.IdProduit = dr.GetInt32(dr.GetOrdinal("idproduit"));
                    produit.IdConditionConservation = dr.GetInt32(dr.GetOrdinal("idconditionconservation"));
                    produit.IdSousCategorie = dr.GetInt32(dr.GetOrdinal("idsouscategorie"));
                    produit.NomProduit = dr.GetString(dr.GetOrdinal("nomProduit"));
                    list.Add(produit);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Erreur lors de la récuparation de toutes les produits : " + ex.Message);
            }
            return list;
        }
        public List<Produit> GetAllById(int id)
        {
            List<Produit> list = new List<Produit>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "select * from produit where idsouscategorie = @idsouscategorie;";
            cmd.Parameters.Add(new MySqlParameter("idsouscategorie", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Produit produit = new Produit();
                    produit.IdProduit = dr.GetInt32(dr.GetOrdinal("idproduit"));
                    produit.IdConditionConservation = dr.GetInt32(dr.GetOrdinal("idconditionconservation"));
                    produit.IdSousCategorie = dr.GetInt32(dr.GetOrdinal("idsouscategorie"));
                    produit.NomProduit = dr.GetString(dr.GetOrdinal("nomProduit"));
                    list.Add(produit);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation de la liste de produit idsouscategorie = " + id + " : " + ex.Message);
            }
            return list;
        }
        public Produit GetById(int id)
        {
            Produit prod = new Produit();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "select * from produit where idproduit = @idproduit;";
            cmd.Parameters.Add(new MySqlParameter("idproduit", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    prod.IdProduit = dr.GetInt32(dr.GetOrdinal("idproduit"));
                    prod.IdConditionConservation = dr.GetInt32(dr.GetOrdinal("idconditionconservation"));
                    prod.IdSousCategorie = dr.GetInt32(dr.GetOrdinal("idsouscategorie"));
                    prod.NomProduit = dr.GetString(dr.GetOrdinal("nomProduit"));
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation du produit idproduit = " + id + " : " + ex.Message);
            }
            return prod;
        }
    }
}
