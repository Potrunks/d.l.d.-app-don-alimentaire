﻿using Fr.EQL.AI110.DLD_GE.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.DataAccess
{
    public class SouscategorieDAO : DAO
    {
        public List<Souscategorie> GetAll()
        {
            List<Souscategorie> list = new List<Souscategorie>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "select * from souscategorie;";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Souscategorie souscategorie = new Souscategorie();
                    souscategorie.IdSousCategorie = dr.GetInt32(dr.GetOrdinal("idsouscategorie"));
                    souscategorie.IdCategorie = dr.GetInt32(dr.GetOrdinal("idcategorie"));
                    souscategorie.Nom = dr.GetString(dr.GetOrdinal("nom"));
                    list.Add(souscategorie);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation de toutes les sous catégories : " + ex.Message);
            }
            return list;
        }
        public List<Souscategorie> GetAllById(int id)
        {
            List<Souscategorie> list = new List<Souscategorie>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "select * from souscategorie where idcategorie = @idcategorie;";
            cmd.Parameters.Add(new MySqlParameter("idcategorie", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Souscategorie souscategorie = new Souscategorie();
                    souscategorie.IdSousCategorie = dr.GetInt32(dr.GetOrdinal("idsouscategorie"));
                    souscategorie.IdCategorie = dr.GetInt32(dr.GetOrdinal("idcategorie"));
                    souscategorie.Nom = dr.GetString(dr.GetOrdinal("nom"));
                    list.Add(souscategorie);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation de la liste de sous-categorie idsouscategorie = " + id + " : " + ex.Message);
            }
            return list;
        }
        public Souscategorie GetById(int id)
        {
            Souscategorie subCat = new Souscategorie();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "select * from souscategorie where idsouscategorie = @idsouscategorie;";
            cmd.Parameters.Add(new MySqlParameter("idsouscategorie", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    subCat.IdSousCategorie = dr.GetInt32(dr.GetOrdinal("idsouscategorie"));
                    subCat.Nom = dr.GetString(dr.GetOrdinal("nom"));
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation de la sous-categorie idcategorie = " + id + " : " + ex.Message);
            }
            return subCat;
        }
    }
}
