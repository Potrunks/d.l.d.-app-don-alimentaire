﻿using Fr.EQL.AI110.DLD_GE.Business;
using Fr.EQL.AI110.DLD_GE.Entities;
using Microsoft.AspNetCore.Mvc;
using Fr.EQL.AI110.DLD_GE.WebApp2.Models;

namespace Fr.EQL.AI110.DLD_GE.WebApp2.Controllers
{
    public class RechercheController : Controller
    {
        [HttpGet]
        public IActionResult RechercheDonMultiCri()
        {
            // récupérer le modele
            DonRechercheViewModel model = new DonRechercheViewModel();

            CategorieBusiness catBU = new CategorieBusiness();
            model.Categories = catBU.GetCategories();


            SouscategorieBusiness souscatBU = new SouscategorieBusiness();
            model.Souscategories = souscatBU.GetSousCategories();

            ProduitBusiness prodBU = new ProduitBusiness();
            model.Produits = prodBU.GetProduits();

            VilleBusiness villeBU = new VilleBusiness();
            model.Villes = villeBU.GetVilles();

            // charger la vue :
            return View(model);
        }
        [HttpPost]
        public IActionResult RechercheDonMultiCri(DonRechercheViewModel model)
        {
            // récupérer le modele
            DonBusiness donBU = new DonBusiness();
            List<DonDetails> resultats = donBU.RechercheDonMultiCri(model.Nomcategorie, model.Nomsouscategorie, model.NomProduit, model.Nomville);
            

            // charger la vue :
            return View("Resultat", resultats);
        }
    }
}
