﻿using Fr.EQL.AI110.DLD_GE.Entities;
namespace Fr.EQL.AI110.DLD_GE.WebApp2.Models
{
    public class DonRechercheViewModel
    {   //données pour afficher
        public List<Categorie> Categories { get; set; }

        public List<Souscategorie> Souscategories { get; set; }
        public List<Produit> Produits { get; set; }
        public List<Ville> Villes { get; set; }
        //données pour rechercher
        public string Nomcategorie { get; set; }

        public string Nomsouscategorie { get; set; }
        public string NomProduit { get; set; }
        public string Nomville { get; set; }

        public DonRechercheViewModel(string nomcategorie, string nomsouscategorie, string nomProduit, string nomville)
        {
            Nomcategorie = nomcategorie;
            Nomsouscategorie = nomsouscategorie;
            NomProduit = nomProduit;
            Nomville = nomville;
        }

        public DonRechercheViewModel()
        {
        }
    }
}
